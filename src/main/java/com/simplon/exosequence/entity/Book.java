package com.simplon.exosequence.entity;

import java.util.Set;

import com.simplon.exosequence.exception.NoDuplicate;

public class Book {
  private int id;

  private String title;

  private String author;

  private Set<BookCopy> copies;

  public Book() {
  }

  public Set<BookCopy> getCopies() {
    return copies;
  }

  public void setCopies(Set<BookCopy> copies) {
    this.copies = copies;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Book(String title, String author) {
    this.title = title;
    this.author = author;
  }

  public Book(int id, String title, String author) {
    this.setId(id);
    this.title = title;
    this.author = author;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public BookCopy checkAvailable() {
    for (BookCopy bookCopy : copies) {
      if (bookCopy.isAvailable()) {
        return bookCopy;
      }
    }
    return null;
  }

  public boolean borrow(User user) throws NoDuplicate {
    if (user.getId() > 0 && user.checkMembership()) {
      BookCopy copy = this.checkAvailable();
      if (copy != null && !user.getBorrowed().contains(copy)) {
        copy.setBorrower(user);
        return true;
      }
      return false;
    } else {
      throw new NoDuplicate("You cannot have multiple copies of one book");
    }
  }

}
