package com.simplon.exosequence.entity;

import java.util.Date;
import java.util.Set;

public class User {
  private int id;

  private String username;

  private Date membershipExpire;

  private boolean membershipSuspended;

  private Set<BookCopy> borrowed;

  public User(int id, String username, Date membershipExpire, boolean membershipSuspended) {
    this.id = id;
    this.username = username;
    this.membershipExpire = membershipExpire;
    this.membershipSuspended = membershipSuspended;
  }

  public Set<BookCopy> getBorrowed() {
    return borrowed;
  }

  public void setBorrowed(Set<BookCopy> borrowed) {
    this.borrowed = borrowed;
  }

  public User(String username, Date membershipExpire, boolean membershipSuspended) {
    this.username = username;
    this.membershipExpire = membershipExpire;
    this.membershipSuspended = membershipSuspended;
  }

  public User() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Date getMembershipExpire() {
    return membershipExpire;
  }

  public void setMembershipExpire(Date membershipExpire) {
    this.membershipExpire = membershipExpire;
  }

  public boolean isMembershipSuspended() {
    return membershipSuspended;
  }

  public void setMembershipSuspended(boolean membershipSuspended) {
    this.membershipSuspended = membershipSuspended;
  }

  public boolean checkMembership() {
    if (membershipSuspended != true && new Date().before(membershipExpire)) {
      return true;
    }
    return false;
  }
}
