package com.simplon.exosequence.entity;

public class BookCopy {
  private int id;

  private String location;

  private User borrower;

  private Book book;

  public BookCopy() {
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

  public User getBorrower() {
    return borrower;
  }

  public void setBorrower(User borrower) {
    this.borrower = borrower;
  }

  public BookCopy(String location) {
    this.location = location;
  }

  public BookCopy(int id, String location) {
    this.id = id;
    this.location = location;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public boolean isAvailable() {
    if (borrower == null) {
      return true;
    }
    return false;
  }
}
