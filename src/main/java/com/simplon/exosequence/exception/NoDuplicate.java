package com.simplon.exosequence.exception;

public class NoDuplicate extends Exception {

  public NoDuplicate(String arg0) {
    super(arg0);
  }

}
